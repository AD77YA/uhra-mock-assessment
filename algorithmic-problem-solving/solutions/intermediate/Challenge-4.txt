You can search for a target value in a rotated sorted array using a modified binary search algorithm. Here's a Python program to do that:
def search_rotated_array(nums, target):
    left, right = 0, len(nums) - 1

    while left <= right:
        mid = left + (right - left) // 2

        if nums[mid] == target:
            return mid  # Found the target

        if nums[left] <= nums[mid]:
            # Left half is sorted
            if nums[left] <= target < nums[mid]:
                right = mid - 1
            else:
                left = mid + 1
        else:
            # Right half is sorted
            if nums[mid] < target <= nums[right]:
                left = mid + 1
            else:
                right = mid - 1

    return -1  # Target not found

# Example usage:
nums = [4, 5, 6, 7, 0, 1, 2]
target = 0
result = search_rotated_array(nums, target)
if result != -1:
    print(f"Target {target} found at index {result}.")
else:
    print("Target not found in the rotated sorted array.")

Explaination:
In this program, we use a modified binary search to search for the target value in the rotated sorted array. 
The key observation is that one half of the array is still sorted, even after rotation.
We check which half is sorted and whether the target value falls within the sorted portion. 
If it does, we continue searching in that sorted half; otherwise, we search in the other half.
This algorithm has a time complexity of O(log n), where n is the size of the rotated sorted array.