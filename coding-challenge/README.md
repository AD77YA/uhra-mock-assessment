# UHRA Coding Challenges

Welcome to the UHRA Coding Challenges Mock-Asssessment! Here, you'll find a set of coding challenges designed to assess your skills in programming related to autonomous vehicles and Formula Student competitions.

## How to Participate

To participate in these challenges, follow these steps:

1. **Fork this repository** to your own GitLab account.

2. **Clone your forked repository** to your local machine:

```git clone https://gitlab.com/your-username/uhra-assessment/coding-challenge.git```

3. Inside the repository, you'll find a set of coding challenges categorized by difficulty level: Beginner, Intermediate, and Advanced.

4. **Choose a challenge** that matches your skill level and interest.

5. **Solve the challenge** by writing Python code a Python file (e.g., `Solutions/challenge-1-John-Simth.py`, `Solutions/challenge-2-Emma-Snow.py`, etc.). Include comments to explain your approach.

6. **Commit your completed challenge** to your local repository in desingated difficulty level folder inside solutions directory:

```git add challenge-X-your-name.py # Replace X with the challenge number```

```git commit -m "Completed Challenge X - Your Name" # Replace X with the challenge number```

7. **Push your changes** to your GitHub fork:

```git push origin main```

8. **Create a Pull Request (PR)** from your forked repository's main branch to the `main` branch of this original repository.

9. In the PR description, provide a brief summary of your solution and any insights gained during the challenge.

10. **Your Answers will NOT be reviewed**, and The answer to these questions shall be pushed to Main Repository A DAY before the ASSESSMENT.

## Challenge Descriptions

### Beginner Level Challenges

1. **Challenge 1:**: Write a Python function find_duplicates(nums) that takes a list of integers as input and returns a list of all the integers that appear more than once in the input list. The order of elements in the output list does not matter.

2. **Challenge 2:**: Write a Python function find_missing_numbers(list1, list2) that takes two lists as input and returns a new list containing the elements that are in list1 but not in list2, without any duplicates.

### Intermediate Level Challenges

3. **Challenge 3:**: Write a Python program to find all prime numbers within a given range of numbers (between a minimum and a maximum value).

4. **Challenge 4:**: Write a Python program that takes a list of numbers as input and calculates the mean (average) of the numbers in the list.

### Advanced Level Challenge

5. **Challenge 5:**: Write a Python function count_palindromic_substrings(s) that takes a string s as input and returns the total number of palindromic substrings in the given string. A palindromic substring is a substring that reads the same forwards as it does backward.

## Have Fun and Learn!

These challenges offer an opportunity to test your programming skills in the context of Formula Student AI. Enjoy the challenges, learn from them, and showcase your abilities. Good luck, and may the Force be with you.
