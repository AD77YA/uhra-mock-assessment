You can write a Python function to find duplicates in a list of integers by using a dictionary to keep track of the frequency of each element. Here's a sample implementation of the `find_duplicates` function:

```python
def find_duplicates(nums):
    num_counts = {}  # Dictionary to store the frequency of each element
    duplicates = []

    for num in nums:
        if num in num_counts:
            num_counts[num] += 1
        else:
            num_counts[num] = 1

    for num, count in num_counts.items():
        if count > 1:
            duplicates.append(num)

    return duplicates

# Example usage:
input_list = [1, 2, 3, 2, 4, 5, 4, 6, 7]
result = find_duplicates(input_list)
print("Duplicates in the list:", result)
```

In this code, we first iterate through the input list (`nums`) and use a dictionary (`num_counts`) to keep track of the frequency of each element. If we encounter an element that's already in the dictionary, we increment its count. After processing all elements, we iterate through the dictionary to find elements with counts greater than 1, indicating that they are duplicates. The duplicates are then collected in a list and returned as the result.

The example usage will print the duplicates in the input list, which will be `[2, 4]` in this case.